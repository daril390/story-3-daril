from django.urls import path
from . import views


app_name = 'homepage'

urlpatterns = [
    path('', views.index, name="index"),
    path('education/', views.education, name="education"),
    path('experience/', views.experience, name="experience"),
    path('project/', views.project, name="project"),
    path('schedule/create/', views.schedule_create_view, name="create"),
    path('schedule/', views.view_skejul, name = "view"),
    path('reset', views.reset, name = "reset"),
    path('delete/<pk>', views.delete, name = "delete")
]
