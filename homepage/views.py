from django.shortcuts import render, redirect

from .forms import ScheduleForm

from .models import Schedule

# Create your views here.
def index(request):
    return render(request, 'index.html')

def education(request):
    return render(request, 'education.html')

def experience(request):
    return render(request, 'experience.html')

def project(request):
    return render(request, 'project.html')

def schedule_create_view(request):
    form = ScheduleForm(request.POST or None)
    if form.is_valid():
        form.save()
        form = ScheduleForm()
    context = {
        'form': form
    }

    return render(request, "schedule/schedule_create.html", context)

def view_skejul(request):
    skejuls = Schedule.objects.all()
    return render(request, 'schedule/schedule_view.html', {
        'skejuls': skejuls
    })

def reset(request):
    schedule = Schedule.objects.all()
    schedule.delete()
    return redirect('homepage:view')

def delete(request, pk):
    print(pk)
    schedule = Schedule.objects.get(pk=pk)
    schedule.delete()
    return redirect('homepage:view')
