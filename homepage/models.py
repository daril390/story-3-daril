from django.db import models
from datetime import datetime, date
from django import forms
from django.utils import timezone

# Create your models here.

class Schedule(models.Model):
    activity        = models.CharField(max_length=120)
    place           = models.CharField(max_length=120)
    dateTime        = models.DateTimeField(default=timezone.now)
    category        = models.CharField(max_length=120)